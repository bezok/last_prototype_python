from Box2D import b2ContactListener, b2World
from collections import deque
import network
import time



class World:
   def __init__(self):
      print("Initialize world")
      self.actions = deque()
      self.b2_world = b2World()
      self.b2_world_update_time = 0
      self.current_time = time.time()
      self.entities = {}
      self.entity_id = 0
      self.entity_update_time = 0.0



   def add_entity(self, entity):
      self.entities[entity.entity_id] = entity
      self.actions.append(network.PacketAddEntity(entity))



   def delete_entity(self, entity_id):
      self.actions.append(network.PacketDeleteEntity(entity_id))
      del self.entities[entity_id]


   
   def get_entity_id(self):
      self.entity_id += 1
      return self.entity_id



   def start(self):
      pass



   def update(self):
      self.current_time = time.time()
      self.update_entity()
      self.update_b2_world()
      


   def update_b2_world(self):
      if self.current_time - self.b2_world_update_time >= 1.0/30.0:
         self.b2_world.Step(1.0/30.0, 6, 2)



   def update_entity(self):
      if self.current_time - self.entity_update_time >= 1.0/30.0:
         for i, entity in self.entities.items():
            entity.update()
         self.entity_update_time = self.current_time



class ContactListener(b2ContactListener):
   def __init__(self):
      b2ContactListener__init__(self)




