from network import Network
from world import World
import asyncio
import time



class Server:
   def __init__(self):
      print("Initialize server")
      self.current_time = 0.0
      self.world = World()
      self.network = Network(self.world)



   @asyncio.coroutine
   def main_loop(self):
      while True:
         self.current_time = time.time()
         self.world.update()
         yield from asyncio.sleep(1.0/30.0)



   def start(self):
      print("Start server")

      loop = asyncio.get_event_loop()

      asyncio.async(self.network.run())
      asyncio.ensure_future(self.network.handle_packets())
      asyncio.ensure_future(self.main_loop())

      loop.run_forever()




if __name__ == "__main__":
   server = Server()
   server.start()
