from collections import deque
import asyncio
import creature
import json
import time
import websockets



class Network:
   def __init__(self, world):
      self.clients = {}
      self.current_time = time.time()
      self.world = world



   @asyncio.coroutine
   def connection_handler(self, websocket, port):
      try:
         client = Client(self.world)
         self.clients[websocket] = client
         while True:
            packet = yield from websocket.recv()
            client.received.append(packet)
            print(packet)
            yield from asyncio.sleep(1.0/30.0)
      except:
         del self.clients[websocket]



   @asyncio.coroutine
   def handle_packets(self):
      while True:
         self.handle_received_packets()
         packet = self.make_packet()
         yield from self.send_packet(packet)
         yield from asyncio.sleep(1.0/30.0)



   def handle_received_packets(self):
      pass



   def make_packet(self):
      packet = {}
      packet["a"] = self.make_packet_actions()
      packet["u"] = self.make_packet_update()
      return json.dumps(packet)



   def make_packet_actions(self):
      packet = []

      while True:
         if (self.world.actions):
            action = self.world.actions.popleft()
            data = action.pack()
            packet.append(data)
         else:
            return packet



   def make_packet_update(self):
      packet = PacketUpdateEntity(self.world).pack()
      return packet



   def send_packet(self, packet):
      for websocket in self.clients:
         yield from websocket.send(packet)



   def run(self):
      self.websocket = websockets.serve(self.connection_handler, 'localhost', 53011)
      return self.websocket





class Client:
   def __init__(self, world):
      self.creatures = {}
      self.received = deque()





class Packet:
   def execute(self):
      pass



   def pack(self):
      pass





class PacketAddEntity(Packet):
   def __init__(self, entity):
      self.entity = entity



   def pack(self):
      data = {}
      data["id"] = self.entity.entity_id
      data["x"] = self.entity.body.position.x
      data["y"] = self.entity.body.position.y
      data["type"] = self.entity.entity_type
      return data





class PacketDeleteEntity(Packet):
   def __init__(self, entity_id):
      self.entity_id = entity_id



   def pack(self):
      data = {}
      data["a"] = "dc"
      data["id"] = self.entity_id
      return data





class PacketUpdateEntity(Packet):
   def __init__(self, world):
      self.world = world



   def pack(self):
      packet = []
      for i, entity in self.world.entities.items():
         data = {}
         data["id"] = entity.entity_id
         data["x"] = entity.body.position.x
         data["y"] = entity.body.position.y
         data["type"] = entity.entity_type
         packet.append(data)
      return packet







