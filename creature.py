from Box2D import *
from entity import Entity



class Creature(Entity):
   def __init__(self, world):
      Entity.__init__(self, world)
      self.alive = True
      self.attack_to = 0
      self.group = 0
      self.health = 100.0
      self.last_attack_time = 0
      self.last_move_time = 0
      self.move_to = b2Vec2()

      self.make_body()



   def attack(self):
      self.world.entities[self.attack_to].take_hit(self.hit())



   def hit(self, damage):
      return self.strength



   def make_body(self):
      body_def = b2BodyDef()
      body_def.type = b2_dynamicBody
      self.body = self.world.b2_world.CreateBody(body_def)
      fixture_def = b2FixtureDef()

      shape = b2PolygonShape(box=(0.5, 0.5))

      fixture_def.shape = shape
      self.body.CreateFixture(fixture_def)


   
   def move(self):
      vector = b2Vec2(self.move_to - self.body.position)
      force = vector * vector.length * self.agility*0.0045
      self.body.ApplyForceToCenter((force.x, force.y), True)



   def set_attack(self, entity_id):
      self.attack_to = entity_id



   def set_creature(self):
      self.set_body()



   def set_move(self, x, y):
      self.move_to.x = x
      self.move_to.y = y



   def take_hit(self, damage):
      self.health -= damage
      if self.health <= 0.0:
         self.alive = False
      self.world.actions.append({"a": "h", "id": self.entity_id})



   def update(self):
      if self.alive:
         #self.update_attack()
         #self.update_health()
         self.update_move()
         print("update")



   def update_attack(self):
      if self.world.current_time - self.last_attack >= 5.0 - self.agility*0.0045:
         self.attack()



   def update_health(self):
      if self.world.current_time - self.last_attack >= 1.0:
         if self.health < self.durability:
            self.health += 1



   def update_move(self):
      if self.world.current_time - self.last_move_time >= 5.0 - self.agility*0.0045:
         if self.move_to != self.body.position:
            self.move()





class Human(Creature):
   def __init__(self):
      Creature.__init__(self)
      self.agility = 100
      self.durability = 100
      self.strength = 100
      self.vitality = 100











