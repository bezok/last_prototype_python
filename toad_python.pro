TEMPLATE = app
QT = core
CONFIG += console
CONFIG -= app_bundle
CONFIG += c++11

SOURCES +=

DISTFILES += \
    creature.py \
    entity.py \
    network.py \
    server.py \
    weapon.py \
    world.py \
    settings.py
